#!/usr/bin/env python3

import subprocess, sys, json, xml.etree.ElementTree as ET, datetime, pathlib

spotify_url = sys.argv[1]
output_file = pathlib.Path(sys.argv[2])

spotify_json = json.loads(subprocess.run(['youtube-dl', '--dump-single-json', spotify_url], capture_output=True).stdout)

feed = ET.Element('feed', {'xmlns': 'http://www.w3.org/2005/Atom'})

ET.SubElement(feed, 'id').text = spotify_json['webpage_url']
ET.SubElement(feed, 'link', {'href': spotify_json['webpage_url']})
ET.SubElement(feed, 'title').text = spotify_json['title']
ET.SubElement(feed, 'subtitle').text = spotify_json['description']

updated_element = ET.SubElement(feed, 'updated')
updated = datetime.datetime.min

icon_element = ET.SubElement(feed, 'icon')
logo_element = ET.SubElement(feed, 'logo')
icon = None

for spotify_entry in spotify_json['entries']:
	entry = ET.SubElement(feed, 'entry')
	ET.SubElement(entry, 'title').text = spotify_entry['title']

	release_date = datetime.datetime.strptime(spotify_entry['release_date'], '%Y%m%d')
	ET.SubElement(entry, 'published').text = release_date.isoformat()

	ET.SubElement(entry, 'icon').text = spotify_entry['thumbnail']

	ET.SubElement(entry, 'link', {'href': spotify_entry['url']})

	if release_date > updated:
		updated = release_date
		icon = spotify_entry['thumbnail']

updated_element.text = updated.isoformat()
icon_element.text = icon
logo_element.text = icon

update = False
try:
	file_date = datetime.datetime.fromtimestamp(output_file.stat().st_mtime)
	update = updated > file_date
except FileNotFoundError:
	update = True

if update:
	ET.ElementTree(feed).write(output_file.open('wb'))
